#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" A simple program to generate wiggly lines based on the YouTube video
https://youtu.be/4CbPksEl51Q
"""

import numpy as np
import math
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt


Ax = 0.0    # Turntable 1 x position
Ay = 0.0    # Turntable 1 y position
Ar = 1.0    # Turntable 1 radius

Bx = 3.0    # Turntable 2 x position
By = 0.0    # Turntable 2 y position
Br = 1.0    # Turntable 2 radius

SpeedRatio = 3      # Ratio od Turntable 2 to 1
StickLength = 2.5   # Length of the sticks connected to the turntables
Offset = 0          # Offset in Radians between turntable 2 and 1

tStart = 0.0
tStep = 0.1
tEnd = 2 * math.pi  # One revoloution of turntable 1

frame = 0

def A_x(r1):
    """Return the x position of turntable 1"""
    return Ax + Ar * math.cos(r1)


def A_y(r1):
    """Return the y position of turntable 1"""
    return Ay + Ar * math.sin(r1)


def B_x(r1):
    """Return the x position of turntable 2"""
    return Bx + Br * math.cos(SpeedRatio * r1 + Offset)


def B_y(r1):
    """Return the y position of turntable 2"""
    return By + Br * math.sin(SpeedRatio * r1 + Offset)


def M_x(r1):
    """Return the mid x of the two turntables"""
    return (A_x(r1) + B_x(r1)) / 2.0


def M_y(r1):
    """Return the mid y of the two turntables"""
    return (A_y(r1) + B_y(r1)) / 2.0


def Beta(t):
    return math.atan((M_y(t) - B_y(t)) / (M_x(t) - B_x(t)))


def Gamma(t):
    return math.acos(math.sqrt((M_y(t) - B_y(t))**2 + (M_x(t) - B_x(t))**2) /
                     (2 * StickLength))


def P_x(t):
    return A_x(t)+StickLength*math.cos(Beta(t)+Gamma(t))


def P_y(t):
    return A_y(t)+StickLength*math.sin(Beta(t)+Gamma(t))


def genimage(v, o):
    global SpeedRatio
    global Offset
    global frame
    print(" -> Working on frame: {}".format(frame), end="\r", flush=True)
    SpeedRatio = v
    Offset = o
    time = np.linspace(tStart, tEnd, 10000)
    Px = []
    Py = []
    Mx = []
    My = []
    Bx = []
    By = []
    Ax = []
    Ay = []

    for t in time:
        Px.append(P_x(t))
        Py.append(P_y(t))
        Mx.append(M_x(t))
        My.append(M_y(t))
        Bx.append(B_x(t))
        By.append(B_y(t))
        Ax.append(A_x(t))
        Ay.append(A_y(t))

    fig, ax = plt.subplots(figsize=(19.2,10.8))
    line1, = ax.plot(Px, Py, linewidth=2, label='Final')
    #line2, = ax.plot(Mx, My, linewidth=2, label='Mid')
    #line3, = ax.plot(Bx, By, linewidth=2, label='B')
    #line4, = ax.plot(Ax, Ay, linewidth=2, label='A')



    #ax.axis('equal')
    plt.xlim(-1.25,3.25)
    plt.ylim(1,3.5)
    mytitle = "Speed Ratio: {:.2f} Offset: {:.2f}".format(SpeedRatio, Offset)
    plt.title(mytitle)
    ax.legend(loc='lower right')
    fig.savefig("frames/test-{:04d}.png".format(frame))
    frame += 1
    plt.close()

def main():
    vs = np.linspace(1, 3, 240)
    for v in vs:
        genimage(v, 0)
    os = np.linspace(0, 2 * math.pi, 240)
    for o in os:
        genimage(3,o)
    print("All done!")

if __name__ == "__main__":
    main()
